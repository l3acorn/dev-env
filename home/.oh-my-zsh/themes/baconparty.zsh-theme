# vim:ft=zsh ts=2 sw=2 sts=2                                                                    

PROMPT='%(?,%{$fg[green]%},%{$fg[red]%}) %% '
# RPS1='%{$fg[blue]%}%~%{$reset_color%} '
#RPS1='%{$fg[white]%}%2~$(git_prompt_info)%{$fg_bold[black]%}%n%{$reset_color%}@%{$fg[magenta]%}%m%{$reset_color%} $(battery_remaining_prompt)'
RPS1='%{$fg[white]%}%2~$(git_prompt_info)%{$fg_bold[blue]%}%n%{$reset_color%}@%{$fg[magenta]%}%m%{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX=" %{$fg[blue]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN=" %{$fg[black]%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_bold[yellow]%} ☡ %{$reset_color%}"
