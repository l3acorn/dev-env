export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="baconparty"

TERM=xterm-256color

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"
# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git battery zsh-syntax-highlighting)

# User configuration

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
export PATH="/usr/local/sbin:$PATH"

# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# colors for less/man
export LESS_TERMCAP_mb=$(printf '\e[01;31m') # enter blinking mode - red
export LESS_TERMCAP_md=$(printf '\e[01;35m') # enter double-bright mode - bold, magenta
export LESS_TERMCAP_me=$(printf '\e[0m') # turn off all appearance modes (mb, md, so, us)
export LESS_TERMCAP_se=$(printf '\e[0m') # leave standout mode    
export LESS_TERMCAP_so=$(printf '\e[01;33m') # enter standout mode - yellow
export LESS_TERMCAP_ue=$(printf '\e[0m') # leave underline mode
export LESS_TERMCAP_us=$(printf '\e[04;36m') # enter underline mode - cyan

# You may need to manually set your language environment
# export LANG=en_US.UTF-8
# yes, for mosh
LANG='C.UTF-8'
LC_ALL='C.UTF-8'

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"


# aliases 
alias sl='echo "is spelled ls you drunk bastard"; ls'
alias c='clear; echo " "; ls -lah'

alias gjupdate='git commit -m update ; git pull; git push;'

# more tmux 
# don't thik this is necessary anymore
#alias tmux="TERM=screen-256color-bce tmux"
alias t='tmux'
#alias fzf='fzf -m'
alias vsh='vagrant ssh'
alias vla="virsh list --all"

disable_caps_lock()
{
  python3 -c 'from ctypes import *; X11 = cdll.LoadLibrary("libX11.so.6"); display = X11.XOpenDisplay(None); X11.XkbLockModifiers(display, c_uint(0x0100), c_uint(2), c_uint(0)); X11.XCloseDisplay(display)'
}

web() 
{
  links -http-proxy $http_proxy -https-proxy $https_proxy $@
}

# something for opening relevent c files
cim()
{
  THESE=""
  for arg in "$@"
  do
    THESE+=`find . -maxdepth 1 -name "$arg*" -not -name "*.o"`
    if [ -n  "$THESE" ]; then;
      THESE+="\n"; fi
  done
  
  if [ -n  "$THESE" ]
  then
    mim `echo $THESE | tr '\n' ' '`
  fi
}

forever()
{
  if [ $# -lt 1 ] 
  then
    printf "${BOLD}usage: ${NORMAL}forever [speed] <command> [ops...]\n"
    echo -e "\tspeed is optional, the default is one second"
    return
  fi
  if hash "$1" 2>/dev/null
  then
    #watch --differences $@
    while true; do $@ ; sleep 1; done
  else
    #watch --differences --interval $1 ${@:2}
    while true; do ${@:2} ; sleep $1; done
  fi
}

# here we take over the world
pwn() {
  if [ $# -lt 1 ]
  then
    printf "${BOLD}usage: ${NORMAL}pwn [command | (ssh)] ${BOLD}<target-file>${NORMAL}"
    printf "\n\twhere ${BOLD}<target-file>${NORMAL} has one thing per line, e.g."
    printf "\n\t\tpi@192.168.1.99"
    printf "\n\t\tpi@192.168.1.98 ...etc"
    echo -e "\n\tif no command is specified ssh is assumed"
    return
  fi

  PWN_COMMAND=''
  TGT_FILE=''
  if hash "$1" 2>/dev/null
  then
    PWN_COMMAND="$1"
    TGT_FILE="$2"
  else
    # we do ssh by default
    PWN_COMMAND="ssh "
    TGT_FILE="$1"
  fi
  if [ ! -f $TGT_FILE ]
  then
    echo -e "\tfile does not exist"
    return
  fi

  printf "\nwith great power"
  for i in `seq 1 6`; do printf "."; sleep .1; done; echo ""
  tmux new-window "$PWN_COMMAND $$TGT_FILE"
  while read i
  do
    tmux split-window -h "$PWN_COMMAND $i"
    tmux select-layout tiled > /dev/null
  done < $TGT_FILE
  tmux select-pane -t 0
  tmux set-window-option synchronize-panes on > /dev/null
}
    
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -d /opt/homebrew/bin/ ] && PATH=$PATH:/opt/homebrew/bin
[ -d ~/.cargo ] && PATH="$PATH:$HOME/.cargo/bin/"
[ -d $HOME/.local/bin ] && PATH="$PATH:$HOME/.local/bin/"
[ -d $HOME/.gem/ruby/2.7.0/bin/ ] && PATH="$PATH:$HOME/.gem/ruby/2.7.0/bin/"
