#!/bin/bash -x

PACKAGES="git ansible"
GIT_REMOTE_URL="https://gitlab.com/l3acorn/dev-env.git"
ANSIBLE_GALAXY_DEPENDS="community.general"

function install_depends() {
  if type apt >/dev/null 2>/dev/null; then
    sudo apt update && sudo apt install -y $PACKAGES || exit $?
    return
  fi
  
  # on rhel-ish
  if type yum >/dev/null 2> /dev/null ; then 
    sudo yum install -y ansible-core git python3-jmespath || exit $?
    return
  fi
  
  # on arch-ish
  if type pacman >/dev/null 2> /dev/null ; then 
    sudo pacman -S $PACKAGES --noconfirm || exit $?
    return
  fi

  echo "Distribution not supported" && exit 2
}

install_depends
if [[ ! -d .git ]]; then
  git clone $GIT_REMOTE_URL ./$(basename $GIT_REMOTE_URL)
  pushd $(basename $GIT_REMOTE_URL)
fi

ansible-galaxy collection install $ANSIBLE_GALAXY_DEPENDS
ansible-playbook site.yml
popd 2>/dev/null || true
