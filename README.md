# DevELOPMENT EnvIRONMENT

## Motivation
In my experience, changes are why we use software in the first place. We build things that are *useful* and they can be *improved* to progress. A development environment should exemplify these principals. **dev-env** (1) installs necessary packages and (2) creates symbolic links in a user's `~/.home` directory back to their counterparts in **dev-env**'s [home](home) directory.

The main advantages to this methodology are:
 - "on-the-fly" changes are tracked, safely and cleanly
 - modification and extension is relatively painless, as everything is Ansible and yml
 - minimal dependencies, currently Ansible is all that is necessary to deploy

For example, let's say you wanted to add a new `zsh` plugin that **dev-env** deploys. The plugin must go in `~/.oh-my-zsh/plugins/new-plugin` on a deployed system. You would make a directory within this repository, `home/.oh-my-zsh/plugins/new-plugin/` and place relevant files within it. The script will create directories and symbolic links as required.

Alternatively consider after **dev-env** has been used to configure a system. A change that is needed can be made on the system's in-use configuration files. The same git rules apply, (e.g. changes need to be checked in), but changes have been made in **dev-env**. I often come back to this project on a system and find that changes have been made. This use-case is the situation I aimed to capture with the creation of this project where under other circumstances those changes would have never been unstreamed.

## Variables
Currently there are three main dictionaries. `core_packages` are things I need and configure but don't take long to install. `extended_packages` are things that take forever to install (mainly latex). `annoying_packages` are packages that are annoying and their absence pleases me. The tasks are pipelined in the playbook so that we start configuring things as the `extended_packages` are installed in the background. 

## Playbook
If you're not familiar with [Ansible](https://www.ansible.com/) I recommend taking some time to look into it as it is very not difficult to learn. This project is mostly Ansible.

The playbook used here is [site.yml](./site.yml). It uses whichever package manager to install system packages and then proceeds to install [oh-my-zsh](https://ohmyz.sh/), my `vim` plugins using [Vundle](https://github.com/VundleVim/Vundle.vim) and some other not-terribly-complicated things that are just nice. Really the only complicated tasks are the ones that recursively create directory structures and the regex to build configuration file symlinks.

## Distributions
Currently, I've tested **dev-env** on Ubuntu 20.20 and Fedora 32.

## Usage

```bash
curl -sL https://gitlab.com/l3acorn/dev-env/-/raw/master/boostrap.sh | bash
```
or

```bash
cd <where you want to put dev-env>
git clone https://gitlab.com/l3acorn/dev-env.git
cd dev-env
./bootstrap.sh
```

Of note, configuration files are linked back to wherever **dev-env** is cloned, so take care of where it is.

